#!/bin/sh

if [ "$1" = "" ] || [ "$2" = "" ]
then
	echo "Usage: $0 INPUT OUTPUT"
	exit 0
fi

echo "Texte: "
read TEXT

DATE=$(date +'%d/%m/%Y')

convert \
	-background None -fill "rgba(70, 70, 70, 0.50)" -pointsize 32 label:"${TEXT} - ${DATE}" \
	-rotate -20 +repage  +write mpr:TILE +delete $1 \
	-alpha set \( +clone -fill mpr:TILE -draw "color 0,0 reset" \) -composite $2

